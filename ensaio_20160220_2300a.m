Ensaio = '20160220'

% Healpix variant
% Configs holds the values for Nside
configs = [1
2
4
8]


%% Variant H1: Healpix with same FOV angles as merid/parallel 
Variant = 'H1';
% Build and plot example sensor
sensor = pd_sensor_build_healpix(2);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor',Variant));	


for FOV = [ pi/4 pi/6 pi/8 pi/12 ]
	for i=1:size(configs,1)
		Nside = configs(i);
		sensor = pd_sensor_build_healpix(Nside);
		graph_plot5(-10, 10, 11, -10, 10, 11, 1, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, Nside=%d, FOV=%3.1f ',...
			Variant, Nside, FOV*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%d_%02.0f',...
			Ensaio,Variant,Nside,FOV*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
	end
end


%% Variant H2: Healpix with FOV angles defined by HEALPix  
Variant = 'H2';
% Build and plot example sensor
sensor = pd_sensor_build_healpix(2);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor',Variant));	


for FOV = [ 58.6 29.3 14.7 7.4 ].*pi/180
	for i=1:size(configs,1)
		Nside = configs(i);
		sensor = pd_sensor_build_healpix(Nside);
		graph_plot5(-10, 10, 11, -10, 10, 11, 1, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, Nside=%d, FOV=%3.1f ',...
			Variant, Nside, FOV*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%d_%02.0f',...
			Ensaio,Variant,Nside,FOV*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
	end
end

