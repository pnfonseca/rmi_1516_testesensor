function plot_pd_target(sensors,z_light,spec)
% function plot_pd_target(sensors,z_light,spec)
%	
% Plot sensor target poionts
% These are the points in the intersection of the light source plane
% and the line defined by the PD direction vector

% Store the current hold state
hold_state = ishold;

% Guarantee that hold is on
hold on 

for i=1:size(sensors,1)
    k = z_light / sensors(i,3);      % k = z_s / z_d
    aim =  k * sensors(i,:);
    plot(aim(1),aim(2),spec)
end

% Return to the previous hold state
if hold_state == 0
	hold off 
end

return