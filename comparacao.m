%
% comparacao.m
%
% Criação de 4 gráficos com valores crescentes de Nparal e Nmerid
% para comparação.
%
% Guarda o resultado em comparacao.eps

 figure
 set(0, 'defaulttextfontsize', 4) 
 subplot(2,2,1)
 graph_plot1(10,40,10,40,1,16,4,60,0)
 subplot(2,2,2)
 graph_plot1(10,40,10,40,1,32,8,60,0)
 subplot(2,2,3)
 graph_plot1(10,40,10,40,1,64,16,60,0)
 subplot(2,2,4)
 graph_plot1(10,40,10,40,1,128,32,60,0)
 print("comparacao.eps","-depsc2")
