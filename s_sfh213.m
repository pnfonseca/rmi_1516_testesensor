function S = s_sfh213(phi)
% function S = s_sfh213(phi)
% 
% Receiving sensibility function for a SFH213 photodiode

% Normalize the angle to -pi:pi
phi = phi - 2*pi*floor( (phi+pi)/(2*pi) );

% The sensitivity function is simetrical wrt the main axis. 
% The interpolation function is defined for positive values only
% Only positive angle values are used in the calculations. 
phi = abs(phi);

% Vectors with sample points for the interpolation function
phi_v = [0  6.1  7.3  8.1 10.8 11.7 12.9 14.1 17.6 23.9 70   90]*pi/180;
beta_v  = [0 18.4 26.6 33.2 45.0 50.8 56.8 63.4 71.6 77.1 89.9 90]*pi/180;

% beta is the result of the abscissa's change of coordinates 
% to use a cosine function to compute the sensibility.
beta = interp1(phi_v, beta_v, phi, 'pchip',pi/2);

% Compute the final value
S = 0.5*(cos(2*beta)+1);
