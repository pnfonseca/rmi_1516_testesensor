function plot_pd_sensor(sensor)
% function plot_pd_sensor(sensor)
% 
% Creates a 3D plot of the orientation vectors for the sensor
%


% Decompose in xyz coords:
x = sensor(:,1);
y = sensor(:,2);
z = sensor(:,3);

t = zeros(size(x));

% plot
quiver3(t,t,t,x,y,z)
