function sensors = pd_sensor_build_healpix(nSide)
% function sensors = pd_sensor_build_healpix(nSide)
% 
% Computes the photo-diode sensor orientation vectors using the
% HEALPix algorithm.
%
% Nside defines the resolution of the grid. NSide must be
% 2^k, k = 0, 1, 2, ...
% HEALPix generates a grid with 12 Nside^2 pixels, 4*Nside being the
% number of pixels in the equator.
% 
% pd_sensor_healpix returns the pixels in the northern hemisphere and
% excluding the pixels in the equator. 
%
% The MEALPIX package requires inputParser, which is implemented only
% in octave 4. The values have been previously generated and stored
% in pd_sensor_HEALPix_n.mat

load pd_sensor_HEALPix_n.mat

switch(nSide)
	case 1
		sensors = pd_sensor_HEALPix1;
	case 2
		sensors = pd_sensor_HEALPix2;
	case 4
		sensors = pd_sensor_HEALPix4;
	case 8
		sensors = pd_sensor_HEALPix8;
	otherwise
		error('invalid nSide value');
end

return
