function estimate = estimate1(light_source, sensors)
% function estimate=estimate1(light_source)
% 
% Computes the estimate of a light source position based on the signal
% received by a photo-diode array sensor. The sensor is a dome, with 
% evenly spaced photo-diodes distributed along meridians (same azimuth,
% different elevation) and parallels (different azimuth, same elevaltion).

% Parameters
FOV = 60;               % Ângulo de visão dos sensors

% Normalização do array de sensors (todos os vetores linha têm norma unitária)
norms = sqrt(sum(sensors.^2,2));
norms = norms(:,ones(1,3)) ;
sensors_norm = sensors ./ norms;

% Criação de matriz com a posição da light_source e mesma dimensão que a matriz
% com os vetores dos fotodíodos
mlight_source = repmat (light_source,size(sensors,1),1);

% Normalização das linhas da matriz mlight_source
norml = sqrt(sum(mlight_source.^2,2));
norml = norml(:,ones(1,3));
mlight_source_norm= mlight_source ./ norml;


% Cálculo dos ângulos de cada sensor para a fonte de light_source:
angulos = acosd(dot(sensors_norm,mlight_source_norm,2));

% estimate da posição da light_source

% m: vetor coluna com os pesos de cada vetor direção dos sensors
% O peso de cada vetor é dado pela intensidade do sinal luminoso recebido
% pelo sensor. Numa primeira versão, por simplificação, considera-se 
% lambertiano com uma função cosseno 
m = cosd(angulos) .* (angulos < FOV);

% A estimate da direção é a média dos vetores direção dos sensors, 
% ponderado pela intensidade do sinal recebido. Alguns dos vetores
% (aqueles cujo ângulo excede o ângulo de visão) têm peso zero.
estimate = sum( m(:,ones(1,3)) .* sensors_norm);
estimate = estimate/norm(estimate);

% Cálculo do erro na estimate
ang_erro = abs(acosd(dot(light_source,estimate,2)/(norm(light_source)*norm(estimate))));

return 