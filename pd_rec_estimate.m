function ligth_intensity = pd_rec_estimate(sensors, light_source, FOV)
% function estimate=estimate1(light_source)
% 
% Computes the estimate of a light source position based on the signal
% received by a photo-diode array sensor. The sensor is a dome, with 
% evenly spaced photo-diodes distributed along meridians (same azimuth,
% different elevation) and parallels (different azimuth, same elevaltion).
% FOV is the photo-diode Field-Of-View in degrees.	

% Normalização do array de sensors (todos os vetores linha têm norma unitária)
norms = sqrt(sum(sensors.^2,2));
norms = norms(:,ones(1,3)) ;
sensors_norm = sensors ./ norms;

% Criação de matriz com a posição da light_source e mesma dimensão que a matriz
% com os vetores dos fotodíodos
mlight_source = repmat (light_source,size(sensors,1),1);

% Normalização das linhas da matriz mlight_source
norml = sqrt(sum(mlight_source.^2,2));
norml = norml(:,ones(1,3));
mlight_source_norm= mlight_source ./ norml;


% Compute the angles between the sensors and the light source direction
angulos = abs(acos(dot(sensors_norm,mlight_source_norm,2)));	

% Estimate of light intensity perceived by the photo-diodes
% ligth_intensity = (cos(angulos)) .* (angulos < FOV);	
ligth_intensity = s_sfh213(angulos);

% a=1;c=15;
% ligth_intensity = a*exp(-(angulos.^2)/(2*c^2)) .* (angulos <= FOV);

return 