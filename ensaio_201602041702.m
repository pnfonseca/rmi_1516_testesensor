% ensaio_201602041702.m
%
% Teste aos problemas com a visualização usando o contourf.
%
% O contourf tem problemas quando a matriz tem NaN. A solução 
% passa por limpá-la primeiro com
% $ angle_error(isnan(angle_error )) = -inf;
% 
% from savannah.gnu.org:
% bug #42766: incorrect color patches for countourf with NaNs in data
%
% (http://stackoverflow.com/questions/26488772/matlab-octave-havent-the-same-result-with-contourf)

load graph_plot2

% Plot the results with surf
figure
subplot(211)
% Use surf() for visualization
surf(xvals, yvals,angle_error);
% contourf seems to cause visualization problems 
% contourf(xvals, yvals,angle_error,0:5:30);
title(sprintf('gp2 Error plot: z_{light}=%d, N_{Merid}=%d, N_{Paral}=%d, FOV=%2.1f^o', ...
z_light, NMerid, NParal,FOV*180/pi));
axis([0 xmax 0 ymax])
xlabel('x');
ylabel('y');
zlabel('Angle error');
view(0,90);
caxis("manual");
% Set color map limits to 0 and 30:
caxis([0 30]);
colorbar();
axis('square');
shading("flat");

angle_error(isnan(angle_error )) = -inf;
% now plot same data with contourf
subplot(212)
contourf(xvals, yvals,angle_error,0:5:30);
title(sprintf('gp2 Error plot: z_{light}=%d, N_{Merid}=%d, N_{Paral}=%d, FOV=%2.1f^o', ...
z_light, NMerid, NParal,FOV*180/pi));
axis([0 xmax 0 ymax])
xlabel('x');
ylabel('y');
zlabel('Angle error');
view(0,90);
caxis("manual");
% Set color map limits to 0 and 30:
caxis([0 30]);
colorbar();
axis('square');
shading("flat");
