%% Preparation

Ensaio = '20160227'

xmin = -10;
xmax = 10; 
xcount = 11;

ymin = -10;
ymax = 10; 
ycount = 11;


% Height (z) of the light source plane
z_light = 3; 

FOV_angles = [ pi/4 pi/6 pi/8 pi/12 pi/20];


%% Sensor with NMerid and NParal

% Sensor configuration: NMerid and NParal
configs = [	8 2
			16 4
			32 8];
 

%% Variant 1, elevation = 0
Variant = '1';
elevation = 0;
% Build and plot example sensor
sensor = pd_sensor_build1(configs(1,1),configs(1,2),0,elevation);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	


elevation = 0;
for FOV = FOV_angles
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
        hh = fopen('max_error.csv','a');
        fprintf(hh,'%d\t%d\t',NM,NP);
        fclose(hh);
        sensor = pd_sensor_build1(NM,NP,0,elevation);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		%print(strcat(basename,'.eps'), '-depsc');
		%print(strcat(basename,'.png'), '-dpng');
        close
    end
end


%% Variant 1, elevation = 15 deg
Variant = '1';
elevation = 15*pi/180;
% Build and plot example sensor
sensor = pd_sensor_build1(configs(1,1),configs(1,2),0,elevation);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	

for FOV = FOV_angles
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build1(NM,NP,0,elevation);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
    end
end



%% Variant 1a, elevation = 15 deg
Variant = '1a';
elevation = 15*pi/180;
% Build and plot example sensor
sensor = pd_sensor_build1a(configs(1,1),configs(1,2),0,elevation);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	

for FOV = FOV_angles
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build1a(NM,NP,0,elevation);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
    end
end

%% Variant 2, elevation = 15 deg
Variant = '2';
elevation = 15*pi/180;
% Build and plot example sensor
sensor = pd_sensor_build2(configs(1,1),configs(1,2),0,elevation,pi/configs(1,1));
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	

for FOV = FOV_angles
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build2(NM,NP,0,elevation,pi/NM);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
    end
end

%% HEALPix section

% Healpix variant
% Configs holds the values for Nside
configs = [1
2
4
8]


%% Variant H1: Healpix with same FOV angles as merid/parallel 
Variant = 'H1';
% Build and plot example sensor
sensor = pd_sensor_build_healpix(2);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor',Variant));	


for FOV = FOV_angles
	for i=1:size(configs,1)
		Nside = configs(i);
		sensor = pd_sensor_build_healpix(Nside);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, Nside=%d, FOV=%3.1f ',...
			Variant, Nside, FOV*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%d_%02.0f',...
			Ensaio,Variant,Nside,FOV*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
	end
end


%% Variant H2: Healpix with FOV angles defined by HEALPix  
Variant = 'H2';
% Build and plot example sensor
sensor = pd_sensor_build_healpix(2);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor',Variant));	


for FOV = [ 58.6 29.3 14.7 7.4 ].*pi/180
	for i=1:size(configs,1)
		Nside = configs(i);
		sensor = pd_sensor_build_healpix(Nside);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, Nside=%d, FOV=%3.1f ',...
			Variant, Nside, FOV*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%d_%02.0f',...
			Ensaio,Variant,Nside,FOV*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
	end
end

%% Variant G: grid sensor
Variant = 'G';
% Build and plot example sensor
sensor = pd_sensor_build_grid(xmin, xmax, xcount, ymin, ymax, ycount, z_light);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor',Variant));	

% Configs holds the number of divisions on each side of the grid
configs = [ 3
    6
    11
    16]

for FOV = FOV_angles
	for i=1:size(configs,1)
		Nside = configs(i);
		sensor = pd_sensor_build_grid(xmin,xmax,Nside, ymin, ymax, Nside, z_light);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, Nside=%d, FOV=%3.1f ',...
			Variant, Nside, FOV*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%d_%02.0f',...
			Ensaio,Variant,Nside,FOV*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
	end
end

%% Variant C: crown sensor
Variant = 'C';
% Build and plot example sensor
sensor = pd_sensor_build_crown(8,2,2,3);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor',Variant));	


configs = [ 8 2
    8 4
    16 4
    16 8];

rc = 8;

for FOV = FOV_angles
	for i=1:size(configs,1)
		Nc = configs(i,1);
        C = configs(i,2);
        
		sensor = pd_sensor_build_crown(Nc,C,rc,z_light);
		graph_plot5(xmin, xmax, xcount, ymin, ymax, ycount, z_light, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, Nc=%d, C=%d, FOV=%3.1f ',...
			Variant, Nc, C, FOV*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%d_%d_%02.0f',...
			Ensaio,Variant,Nc,C,FOV*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
        close;
	end
end


