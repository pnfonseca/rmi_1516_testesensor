function [ result map ] = pd_sensor_build1a(NMerid, NParal, varargin)
% function result = pd_sensor_build1(NMerid, NParal, PMerid, PParal)
%
% Creates the array of sensor versors, indicating the directions 
% of the sensors, given the number of meridians and paralles 
% The constructiong does not include the vertical ("north-pole") sensor.
% The sensor is a hemi-spherical dome, where photo-diodes are regularly
% placed, over NMerid meridians and NParal paralels. The phase arguments
% are optional; if included, the photodiode location will not start at 
% zero, but at the angle NMerid (for azimuth) and NParal (for elevation).
% Note that the effect of phase is different in the azimuth and elevation.
% In azimuth, the phase causes a shift in the position of all sensors. In 
% elevation, the phase is the position of the least elevated sensor and the
% remaining sensors are equally distributed from the initial point and
% pi/2 (not including)
% 
% pd_sensor_build1 returns a map of indexes


% Default values for optional parameters
% Meridian start phase
PMerid = 0;

% Paralel start phase
PParal = 0;

% Processing the input arguments
nVarargs = length(varargin);
if (nVarargs >= 1)
	PMerid = varargin{1};
	if nVarargs >=2
		PParal = varargin{2};
	end
end


% Start building the sensor orientations array and map
result = [];
map = []; 

for m = 0:NMerid-1
	for p = 0:NParal-1
		azimuth = PMerid + m*2*pi/NMerid;
		elevation = PParal + (p*(pi/2-PParal))/(NParal-0.5);
		[vx vy vz] = sph2cart(azimuth,elevation,1);
		result = [result ; vx vy vz];
		map(p+1,m+1)= size(result,1);
	end
end
