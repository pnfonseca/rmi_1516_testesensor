% Geração do gráfico de erro máximo 
% (artigo ETFA 2016)

% max_error.csv foi criado a partir de comandos de escrita
% para ficheiro em ensaio_20160227.m e graph_plot5.m

max_err = dlmread('max_error.csv','\t',1,0)
x = max_err(max_err(:,1) == 32,:,:,:)
plot(x(:,3)*180/pi,x(:,4),'o-')
grid on
xlabel('HFOV / degrees')
ylabel('Max error / m')
title('Max Error for (N_m,N_p) = (32,8)')
print -depsc2 max_error
