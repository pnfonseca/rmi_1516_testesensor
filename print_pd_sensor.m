function print_pd_sensor(sensor)
% function print_pd_sensor(sensor)
% 
% Print the angles (elevation and azimuth) and norm of the direction vectors 
% in a sensor definition array. 

cart2sph (sensor).*[180/pi 180/pi 1]
