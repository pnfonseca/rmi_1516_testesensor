function S=s_cos(angle)
% S=s_cos(angle)
%
% General function for the photo-diode directional sensitivity
% based on a cossine function

S = cos(angle);
return;