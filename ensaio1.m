function ang_erro = ensaio1(luz)
% function ang_erro=ensaio1(luz)
% 
% Cálcula da estimativa da posição de uma fonte luminosa a partir da
% intensidade do sinal luminoso recebido. 
%
% O sensor é uma semi-esfera com fotodíodos regularmente distribuídos.

% Parâmetros
FOV = 60;               % Ângulo de visão dos sensores

% Vetores dos sensores (fotodíodos)
% Não é considerado o 9.º sensor, para fazer um array regular.
sensores = [ 1  0  0
			 0  1  0
			-1  0  0
			 0 -1  0	
			 1  0  1
			 0  1  1
			-1  0  1
			 0 -1  1];

%			0 0 1];

% Normalização do array de sensores (todos os vetores linha têm norma unitária)
norms = sqrt(sum(sensores.^2,2));
norms = norms(:,ones(1,3)) ;
sensores_norm = sensores ./ norms;

% Criação de matriz com a posição da luz e mesma dimensão que a matriz
% com os vetores dos fotodíodos
mluz = repmat (luz,size(sensores,1),1);

% Normalização das linhas da matriz mluz
norml = sqrt(sum(mluz.^2,2));
norml = norml(:,ones(1,3));
mluz_norm= mluz ./ norml;


% Cálculo dos ângulos de cada sensor para a fonte de luz:
angulos = acosd(dot(sensores_norm,mluz_norm,2));

% Estimativa da posição da luz

% m: vetor coluna com os pesos de cada vetor direção dos sensores
% O peso de cada vetor é dado pela intensidade do sinal luminoso recebido
% pelo sensor. Numa primeira versão, por simplificação, considera-se 
% lambertiano com uma função cosseno 
m = cosd(angulos) .* (angulos < FOV);

% A estimativa da direção é a média dos vetores direção dos sensores, 
% ponderado pela intensidade do sinal recebido. Alguns dos vetores
% (aqueles cujo ângulo excede o ângulo de visão) têm peso zero.
estimativa = sum( m(:,ones(1,3)) .* sensores_norm);
estimativa = estimativa/norm(estimativa);

% Cálculo do erro na estimativa
ang_erro = abs(acosd(dot(luz,estimativa,2)/(norm(luz)*norm(estimativa))));

return 