function y = S1_pd(angulo)

	FOV = 60;
	y = cosd(angulo) .* (abs(angulo) < FOV);

return
