function estimate = pd_estimate2(sensors, rec_light, sensor_map)
% function estimate=estimate1(sensors, rec_light)
% 
% Computes the estimate of a light source orientation based on the signal
% received by a photo-diode array sensor. The sensor is a dome, with 
% evenly spaced photo-diodes distributed along meridians (same azimuth,
% different elevation) and parallels (different azimuth, same elevaltion).
%
% pd_estimate2 considers only the maximum value and the surrounding values.
% requires a sensor map.

% Normalização do array de sensors (todos os vetores linha têm norma unitária)
norms = sqrt(sum(sensors.^2,2));
norms = norms(:,ones(1,3)) ;
sensors_norm = sensors ./ norms;

% selected is 1 for rec_light values to be considered
selected = zeros(size(rec_light))	;

% Compute the max value of received light and its position in the array
[m im] = max(rec_light);

% Store in selected: 
selected(im) = 1;


% Convert the position to row and column of sensor_map
[r c] = ind2sub(size(sensor_map),im);

% Search for the max value neighbours
for ri = max(r-1,1):min(r+1,rows(sensor_map))
	for ci = max(c-1, 1): min (c+1, columns(sensor_map))
		selected(sensor_map(ri,ci)) = 0.5;
	endfor
endfor
selected(im) = 1;

selected


rec_light = rec_light .* selected;

% A linha de cima é equivalente a esta...
estimate = rec_light' * sensors;

% Make estimate a unit vector:
estimate = estimate/norm(estimate);

return 