function [ sensor ] = pd_sensor_build_crown ( Nc, C, rc, z_light )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

sensor = [];

for i = C:-1:1

    ri = i/C*rc;
    Ni = round(i/C*Nc);
    for s = 0:(Ni-1)
        alpha = 2*pi*s/Ni;
        [x y z] = pol2cart(alpha, ri, z_light);
        v = [x y z] ./ norm([x y z]);
        sensor = [ sensor; v ];
    end
 
end

