function sobreposicao(N_sensores)
% sobreposicao.m
angulos = 0:90;

delta = 90/(N_sensores)

figure
hold

for pos = linspace(0,90 - delta,N_sensores)
	plot(angulos,S1_pd(angulos - pos))
end

axis([0 90 0 1])
xlabel('Elevation (degrees)')
ylabel('S_{pd}')