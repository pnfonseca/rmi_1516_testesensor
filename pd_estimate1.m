function estimate = pd_estimate1(sensors, rec_light)
% function estimate=estimate1(sensors, rec_light)
% 
% Computes the estimate of a light source orientation based on the signal
% received by a photo-diode array sensor. The sensor is a dome, with 
% evenly spaced photo-diodes distributed along meridians (same azimuth,
% different elevation) and parallels (different azimuth, same elevaltion).

% Normalização do array de sensors (todos os vetores linha têm norma unitária)
norms = sqrt(sum(sensors.^2,2));
norms = norms(:,ones(1,3)) ;
sensors_norm = sensors ./ norms;

estimate = rec_light' * sensors;

% Make estimate a unit vector:
estimate = estimate/norm(estimate);

return 