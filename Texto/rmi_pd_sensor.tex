\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}

\newcommand{\rect}{\text{rect}}
\newcommand{\Nmerid}{\ensuremath{N_{\text{merid}}}}
\newcommand{\Nparal}{\ensuremath{N_{\text{paral}}}}
\newcommand{\degree}{\ensuremath{^\circ}}

\graphicspath{imagens/}

\bibliographystyle{apalike}


\begin{document}

\title{A simple sensor for Visible Light Positioning in Robotics}
\author{Pedro Fonseca}
\date{January 2016}

\maketitle


\section{The sensor}
\label{sec:sensor}

The sensor is a simple structure, used to determine the direction of a
light source. It consists on an array of photo-diodes, regularly
placed on a hemispherical, dome-shaped structure. In this structure,
meridian lines and parallel lines are drawn, the photo-diodes being
placed in the intersection of these lines
(figure~\ref{fig:sens_struct}). The sensor is primarily characterized
by two parameters: $N_{\text{merid}}$ and $N_{\text{paral}}$,
respectively the number of meridians and the number parallels;
figure~\ref{fig:sens_struct} presents and example where
$N_{\text{merid}}=8$ and $N_{\text{paral}}=2$. This sensor would be
placed, for example, in the top of a autonomous mobile robot
(figure~\ref{fig:sensor_robot}) to compute the direction of a light
source relative to the robot.

\begin{figure}
  \centering
  \includegraphics{VLP_sensor_view.eps}
  \caption{Sensor structure}
  \label{fig:sens_struct}
\end{figure}

\begin{figure}
  \centering
  \includegraphics{VLP.eps}
  \caption{Sensor in a robot}
  \label{fig:sensor_robot}
\end{figure}

Reading the light intensity perceived by each photo-diode enables the
sensor to compute an estimate for the light source direction. If the
light source position is known, and considering that the sensor height
is also known, it is possible to estimate the sensor location. If a
single light source is considered, the sensor will yield multiple
results, all located in a circular line, centred in the vertical
projection of the light source position in the robot's plan. By using
a compass or any other means to determine the robot's orientation, the
indetermination can be resolved and the robot location and orientation
can be uniquely determined. With no orientation information available,
the location can be uniquely determined by using multiple light
sources.

\section{Photo-diode model}
\label{sec:photo-diode-model}

The photo-diode model considers that the directional sensitivity is a function $S_\text{pd}(\varphi)$ of the angle $\varphi$ between the photo-diode main axis and the light source direction. For this sensor, we only require $S_\text{pd}$ to be maximal at the photo-diode axis ($\varphi=0$) and decreasing monotonically with increasing $\varphi$, which is a set of conditions fulfilled by most commercial diodes (see figure~\ref{fig:osram} for an example of a commercial photo-diode sensitivity function; in this case, the SFH~213 diodo from OSRAM \cite{osram_opto_semiconductors_gmbh_silicon_2014}). 

\begin{figure}
  \centering
  \includegraphics[scale=0.4]{osram.png}
  \caption{Example of a photo-diode sensitiviy function.}
  \label{fig:osram}
\end{figure}

\section{Estimation of light source position}
\label{sec:estim-light-source}

For the presentation of the estimation algorithms, we will use a sensor with a small number of photo-diodes, in order to facilitate the understanding of the algorithm. We will also consider the sensor to be of size zero; this implies that all photo-diodes are considered to be in the same location in space, the only difference being their orientation.

\subsection{Computation of received light intensity}
\label{sec:comp-rece-light}

We will consider that the light source to be a point source, broadcasting evenly in all directions. This assumption allows to simplify the computations and it can be easily removed, in more complex versions of the model. 

Consider a sensor with $N_\text{merid}=4$ and $N_\text{paral}=2$. This sensor will contain 8 photo-diodes and it will be represented by  the array $\mathbf s$, where each row in $\mathbf s$ is the versor of each photo-diode axis~\eqref{eq:1}. 
\begin{equation}
  \label{eq:1}
  \mathbf s =
  \left[
  \begin{array}{ccc}
    1 & 0 & 0 \\
    \sqrt{2}/2 & 0 & \sqrt{2}/2 \\
    0 & 1 & 0 \\
    0 & \sqrt{2}/2 & \sqrt{2}/2 \\
    -1 & 0 & 0 \\
    -\sqrt{2}/2 & 0 & \sqrt{2}/2 \\
    0 & -1 & 0  \\
    0 & -\sqrt{2}/2 & \sqrt{2}/2 \end{array} \right]
\end{equation}

Each sensor will detect a light intensity, represented by $\mathbf l$. $\mathbf l$ is a column vector, with the same number of lines as $\mathbf s$. Given $\mathbf p$, the position of the light source relative to the sensor, the detected light intensity in each photo-diode $\mathbf l$ is computed by Algorithm~\ref{algo:compute_rec_int}. 
\begin{algorithm}
\caption{Computation of received light intensity} \label{algo:compute_rec_int}
\begin{algorithmic}
  \For {each line $i$ in $\mathbf s$}
  \Comment{Compute the angle between $\mathbf p$ and $\mathbf s[i]$ }
  \State{$\varphi[i]=\arccos\left( {\mathbf s[i] \cdot \mathbf p} \over { \|\mathbf s[i]\| \| \mathbf p \|} \right)$;}
  \State{$\mathbf l[i] = S_\text{pd}(\varphi[i])$}
  \EndFor
\end{algorithmic}
\end{algorithm}

\section{First approximation}
\label{sec:first-approximation}

As the sensitivity function is a decreasing function of the angle $\varphi$ and has its maximum at $\varphi=0$, the light intensity perceived by each photo-diode is an indication of the alignment of the light source position (or, more exactly, of the straight line from the sensor to the light source) with the photo-diode axis. 

The first approximation to the location algorithm estimates the direction of the light source by a weighted sum of the photo-diode direction vectors, the weights being the perceived light intensity by each photo-diode. A necessary condition is that all photo-diode vectors (the lines in $\mathbf s$) have the same length, so that the difference in contributions to the final result is due only to the corresponding weights. In this case, the estimate for the direction of the light source direction, relative to the sensor, is simply:
\begin{equation}
  \label{eq:2}
  \mathbf e^{(1)} = \mathbf l' * \mathbf s 
\end{equation}

\subsection{Simulation}
\label{sec:simulation}

In the simulation experiments, the sensor is placed at the origin, at point $(x,y,z)=(0,0,0)$ and oriented towards the $x$ axis. The light source is placed at several points in a grid over the first quadrant in the $(x,y)$ plane and at a constant height $z$. For a particular sensitivity function $S_{\text{pd}}$, the direction of the light source is estimated and the associated error is computed. 

Following a Lambertian model, we consider the sensitivity function defined in~\eqref{eq:3}, where $\rect(x) = u(x+1/2) - u(x-1/2)$, $u(x)$ being the Heaviside function, and $\theta$ is the photo-diode's field of view (FOV). 

\begin{equation}
  \label{eq:3}
  S_{\text{pd}}(\varphi) = \cos(\varphi) \rect\left({\varphi \over {2 \theta}}\right)
\end{equation}

We will consider a sensor with $\Nmerid = 4$ and $\Nparal = 2$ and photo-diodes with a FOV $\theta=60 ^\circ$. By placing the light source at a height $z=1$ and exploring the region $(0 \le x \le 10, 0 \le y \le 10)$, we get the results in Figure~\ref{fig:exp1}. This configuration yields a maximum error of $32.8 \degree$. Note that these results correspond to a sensor where neighbouring photo-diodes in the horizontal plane are placed at $90\degree$ of each other.  

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth]{imagens/z1NP02NM04FOV60.eps}
  \caption{Error plot with \Nmerid=4, \Nparal=2 and $\theta=60\degree$}
  \label{fig:exp1}
\end{figure}

By increasing the number of meridians to 8, the error reduces to values below $20\degree$, as shown in figure~\ref{fig:expNP04NM08FOV60}. The maximum error is now $19.0\degree$. In a circle of approximately 4m from the sensor, the error is below $15\degree$. 
 
\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth]{imagens/z1NP02NM08FOV60.eps}
  \caption{Error plot with \Nmerid=8, \Nparal=2 and $\theta=60\degree$}
  \label{fig:expNP04NM08FOV60}
\end{figure}

These results show that one of the strategies for reducing the error can be increasing the number of photo-diodes in the sensor. But the solution, {\em per se}, is not a guarantee of better results, as can be seen in figures~\ref{fig:expNP04NM16FOV60} and~\ref{fig:expNP08NM32FOV60}, where the pair $(\Nmerid,\Nparal)$ is increased to $(16,4)$, with a maximum error of $20.5\degree$, and $(32,8)$, with a maximum error of $23.0\degree$, respectively. Further increasing $\Nparal$ and $\Nmerid$ causes the maximum error to increase (Table~\ref{tab:maxerror1}), as well as expanding the regions of larger errors (figure~\ref{fig:increasing_Nparal_Nmerid}). 

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth]{imagens/z1NP04NM16FOV60.eps}
  \caption{Error plot with \Nmerid=16, \Nparal=4 and $\theta=60\degree$}
  \label{fig:expNP04NM16FOV60}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.8\columnwidth]{imagens/z1NP08NM32FOV60.eps}
  \caption{Error plot with \Nmerid=32, \Nparal=8 and $\theta=60\degree$}
  \label{fig:expNP08NM32FOV60}
\end{figure}


\begin{table}
  \centering
  \begin{tabular}{cccc} \hline
    \bf \Nmerid & \bf \Nparal & \bf FOV & \bf Max. error \\ \hline
    4    &  2 & 60 & 32.8 \\ 
    8    &  2 & 60 & 19.0 \\
    16   &  4 & 60 & 20.5 \\
    32   &  8 & 60 & 23.0 \\
    64   & 16 & 60 & 24.7 \\
    128  & 32 & 60 & 25.1 \\ \hline
  \end{tabular}
  \caption{Maximum error as a function of \Nparal and \Nmerid ($\theta = 60\degree$).}
  \label{tab:maxerror1}
\end{table}

\begin{figure}
  \centering
  \includegraphics[angle=90]{imagens/comparacao.eps}
  \caption{Effect of increasing $\Nparal$ and $\Nmerid$.}
  \label{fig:increasing_Nparal_Nmerid}
\end{figure}

The reason for this behaviour can be understood by analysing what happens when the sensor computes the elevation angle using the information from the photo-diodes in a given meridian and the light is placed in the same vertical plane that contains that meridian. Consider that the photo-diodes have a FOV of $60\degree$ and the sensor contains 4 parallels, located at $0\degree$, $22.5\degree$, $45\degree$ and $67.5\degree$. The sensitivity function $S_\text{pd}$ for these four sensors, when the elevation goes from $0\degree$ to $90\degree$, is plotted in Figure~\ref{fig:overlapping_Spd}. It is clear that, for many elevation angles, the sensitivity functions of multiple photo-diodes overlap and all these contribute to the final result. Figure~\ref{fig:error_elev} presents a possible situation: the sensor contains 4 parallels and the light source elevation corresponds to the latitude of the first parallel above equator. In this case, sensor 2 will yield the maximum received power. Sensors 1 and 3 will have similar values for received power; as these two are symmetrical relative to the sensor 2 axis line, the combined result will place the direction estimate on the axis of sensor 2. But sensor 4 can also be detecting power from the light source and, thus, introduce a bias, because, opposed to what happens with sensors 1 and 3, there is no counterpart to balance its contribution. The result will be an error, originating from these unbalanced contributions. Increasing the number of sensors may mean increasing  the number of these unbalanced contributions, thus causing an increasing value of the error, which justifies the results in Figure~\ref{fig:increasing_Nparal_Nmerid}. 

\begin{figure}
  \centering
  \includegraphics[scale=0.4]{imagens/sobreposicao.eps}
  \caption{Overlapping sensitivity functions}
  \label{fig:overlapping_Spd}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[scale=2]{imagens/VLP_Sensor_Assimmetry.eps}
  \caption{Error in elevation.}
  \label{fig:error_elev}
\end{figure}

\bibliography{vlc}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
