#!/usr/bin/octave
#
# Script file to generate the plots of the s_sfh213 function

addpath("../..");

angles = [0:90];

% Vectors with sample points for the interpolation function
alpha_v = [0  6.1  7.3  8.1 10.8 11.7 12.9 14.1 17.6 23.9 70   90];
beta_v  = [0 18.4 26.6 33.2 45.0 50.8 56.8 63.4 71.6 77.1 89.9 90];

figure;

plot(alpha_v,s_sfh213(alpha_v*pi/180),'s','linewidth',4,'markersize',10);

# plot(angles*180/pi,0.5*(cos(2.*angles)+1),'LineWidth',4)
axis([0 100 0 1])
set(gca,'xtick',[0:10:100])
grid on
hold on

plot(angles,s_sfh213(angles*pi/180),'--r','linewidth',4);

print('graph_actual_simul.eps','-depsc')
% print('graph_actual_simul.png','-dpngalpha')
print('graph_actual_simul.emf','-demf')
