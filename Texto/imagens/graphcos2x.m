#!/usr/bin/octave

# figure('visible','off');

angles = [0:90]*pi/180;

# plot(angles*180/pi,0.5*(cos(2.*angles)+1),'LineWidth',4)
axis([0 90 0 1])
set(gca,'xtick',[0 45 90],'ytick',[0 0.5 1])
grid on

title('0.5 cos(2 x)')


print('graphcos2x.eps','-deps')