\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{listings}

\lstset{basicstyle=\footnotesize\ttfamily,breaklines=true}
\lstset{framextopmargin=5pt,frame=single}

\newcommand{\degree}{\ensuremath{^\circ}}

\graphicspath{imagens/}

\bibliographystyle{apalike}


\begin{document}

\title{A simple method to define a numerical model for a photo-diode
  directional sensitivity function}

\author{Pedro Fonseca} 
\date{January 2016}

\maketitle

\begin{abstract}
  Simulating a system based on the detection of light by photo-diodes
  involves modelling the behaviour of this device with respect to
  light intensity perception. In problems related to Visible Light
  Positioning, or in any other where the relative positions of the
  photo-diode and the light source are relevant, the orientation of
  the light source with respect to the photo-diode axis determines the
  measured response of the photo-diode.  We present a simple procedure
  to determine the directional sensitivity function of a photo-diode,
  base on the device's sensitivity graphs in the data-sheet.
\end{abstract}


\section{Introduction}
\label{sec:introduction}

Ignoring all physical causes that contribute to the phenomenon, we can
define the photo-diode as a semiconductor device that generates a
current that depends on the intensity of light radiation that strikes
the photo-diode sensitive surface. The {\em sensitivity} of the
photo-diode is defined as the ratio of generated current to the
incident radiation power and it depends mainly on the radiation
wavelength and on the direction of incidence.  

Commercial photo-diodes are usually encapsulated in
transparent\footnote{Here, transparent is meant at the photo-diode's
  wavelength of operation. The casing of a infra-red photo-diode may
  not be seen by a human as transparent.}  casings that act both as
protective shells and as converging lenses. These lenses determine the
directional characteristics of the photo-diode, which we aim at
simulating, in the problems where the light source position relative
to the photo-diode is relevant. 

Consider the case of a commercial photo-diode, the SFH~213 diodo from
OSRAM \cite{osram_opto_semiconductors_gmbh_silicon_2014}. The
directional characteristics of this photo-diode are presented in
figure~\ref{fig:osram}. This graph presents the relative sensitivity
of the photo-diode w.r.t. the angle between the photo-diode's main
axis and the light source direction. To simulate a system using this
photo-diode, we need to obtain an approximation of this function. 


\begin{figure}
  \centering
  \includegraphics[scale=0.4]{imagens/osram.png}
  \caption{Example of a photo-diode directional sensitiviy function.}
  \label{fig:osram}
\end{figure}


\section{Procedure}
\label{sec:procedure}

Our approximation to the function $S_\text{rel}$ that describes the
directional characteristics of the photo-diode starts by finding a
function that has a shape similar to the directional sensitivity
function. A candidate function should fulfil the following
requirements: 
\begin{itemize}
\item The function must be continuous and differentiable in the
  interval $[0, \pi/2]$;
\item It should start at value $1$ for $x=0$ and then decrease
  monotonically reaching $0$ at $x=\pi/2$;
\item The first derivative should be zero for $x=0$ and $x=\pi/2$.
\end{itemize}

In our case, we consider the function
\[
y = 0.5 \left[ \cos(2 x) + 1 \right]
\]
The graph of this function is presented in
figure~\ref{fig:graphcos2x}. The approximation to the directional
sensitivity function is obtained by performing a variable change in
the $x$ axis in order to shape the cosine function to resemble the
sensitivity function.
\begin{figure}
  \centering
  \includegraphics[width=0.5\columnwidth]{imagens/graphcos2x.eps}
  \caption{Graph of $y = 0.5 \left[ \cos(2 x) + 1 \right]$}
  \label{fig:graphcos2x}
\end{figure}

We need now to define a coordinate transform $f$ from $\varphi$ to $x$
such that the function $y = 0.5 \left[ \cos(2 x) + 1 \right]$, when $x
= f(\varphi)$, will be similar to $S_\text{rel}(\varphi)$. In other
words, we want to find $f$ such that: 
\begin{equation}
  \label{eq:1}
  f : \forall \varphi , S_\text{rel}(\varphi) \approx  0.5 \left[
    \cos(2 \cdot f(\varphi)) + 1 \right]
\end{equation}

For that purpose, we consider a set of interpolation points in the
original $S_\text{rel}$ function. These points are obtained by
measuring relevant distances in the data-sheet graphic and they are
presented in table~\ref{tab:interp_points_solved}, where $\varphi_i$
is the angle of the source light direction with the photo-diode axis
(the abscissa in the graph in figure~\ref{fig:osram}),
$S_{\text{rel},i}$ is the corresponding value for the relative
sensitivity and $x_i$ is the transformed coordinate value, following
the procedure described below. 

% \begin{table}
%   \centering
%   \begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|} \hline
%     $i$                 & 0 & 1 & 2 & 3 & 4  & 5  & 6  & 7  & 8  &  9 \\ \hline
%     $\varphi_i$ & 0 & 6 & 7 & 8 & 11 & 12 & 13 & 14 & 18 & 24 \\ \hline
%     $ S_{\text{rel},i}$      & 1.00 & 0.90 & 0.80 & 0.70 & 0.50 & 0.40 & 0.30 & 0.20 & 0.10 & 0.05 \\ \hline
%   \end{tabular}
%   \caption{Interpolation points}
%   \label{tab:interp_points}
% \end{table}

The function $f$ will make each of the $\varphi_i$ angles in
table~\ref{tab:interp_points_solved} correspond to a value $x_i$ where
$y(x_i)=S_{\text{rel},i}$.  By considering the definition of $y$:
\begin{equation}
  \label{eq:2}
  0.5 \left[  \cos(2 x_i) + 1 \right] = S_{\text{rel},i}
\end{equation}
we get
\begin{equation}
  \label{eq:3}
  x_i = {1 \over 2} \arccos (2 S_{\text{rel},i} -1)
\end{equation}

The value $x_i$ in (\ref{eq:3}) is
$f(\varphi_i)$ and it is presented in the last line of table~\ref{tab:interp_points_solved}.
\begin{table}
  \centering
  \begin{tabular}{||l||c|c|c|c|c|c|c|c|c|c||} \hline\hline
    $i$                 & 0 & 1 & 2 & 3 & 4  & 5  & 6  & 7  & 8  &  9 \\ \hline\hline
    $\varphi_i$         & 0 & 6.1 & 7.3 & 8.1 & 10.8 & 11.7 & 12.9 & 14.1 & 17.6 & 23.9 \\ \hline
    $ S_{\text{rel},i}$   & 1.00 & 0.90 & 0.80 & 0.70 & 0.50 & 0.40 & 0.30 & 0.20 & 0.10 & 0.05 \\ \hline
    $ x_i$              & 0 & 18.4 & 26.6 & 33.2 & 45.0 & 50.8 & 56.8 & 63.4 & 71.6 & 77.1 \\ \hline\hline
  \end{tabular}
  \caption{Interpolation points with results.}
  \label{tab:interp_points_solved}
\end{table}

In order to avoid having to define an analytical function to perform
the change of variables, we make use of the {\tt interp1} function in
Octave (see figure~\ref{fig:s_sfh213}). In the code, {\tt alpha\_v} is
the vector of  sample points for $\varphi_i$ and {\tt beta\_v} the
corresponding $x_i$ values. The two last values in each vector
guarantee that the function converges to zero when $\varphi$
approaches $\pi/2$. 

\begin{figure}
  \centering
  \lstinputlisting[language=Octave]{../s_sfh213.m}
  \caption{Octave code for the directional sensitivity estimate function.}
  \label{fig:s_sfh213}
\end{figure}

The result is shown in figure~\ref{fig:compare}, where the plot of the
computed approximation is shown in red, superposed to the original
directional sensitivity response of the photo-diode, together with the
sampling points used for the interpolation. The numerical
approximation provided by the function in figure~\ref{fig:s_sfh213}
follows closely the original directional sensitivity response of the
photo-diode. Having no numerical data available, visual resemblance is
the criterion that can be applied. 
\begin{figure}
  \centering
  \includegraphics[width=0.9\columnwidth]{imagens/graph_compare.eps}
  \caption{Comparison of simulated with actual response.}
  \label{fig:compare}
\end{figure}

\section{Conclusion}
\label{sec:conclusion}

We have presented a simple procedure for numerically computing the
directional sensitivity of a photo-diode. This procedure allows to
obtain the numerical estimate for a photo-diode's directional
sensitivity function given a set of samples from the original
response, for which an analytical model is not required. 

\bibliography{vlc}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
