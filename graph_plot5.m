function graph_plot5(xmin, xmax, xpoints, ymin, ymax, ypoints, z_light,...
    sensors, Srel, FOV,p)
% Script fot generating surphace graphs for the location sensor error
% 
% Created from graph_plot3.m 
% graph_plot4 considers a phase shift for the meridians (PMerid) and 
% parallels (PParal)
%
% Plots the location error (graph_plot2.m plots the direction error).
%  
% The error is computed by placing the light source on several points 
% in the first quadrant, in an rectangle defined by (0,0)
% and (xmax,ymax). The grid contains xpointx by ypoints. 
% The sensor is placed at (0,0), oriented towards the x axys.
% The light source is placed at the several points in the first quadrant, 
% at a height of z_light.

% Differences to graph_plot4:
% - sensors, the array with the photodiodes orientation vectores, is provided in the
%   argument list 


% h = waitbar(0);
more off

% Compute the points in the x and y axis:
xvals = linspace(xmin, xmax, xpoints);
yvals = linspace(ymin, ymax, ypoints);

% Error computation
angle_error=[];
location_error=[];

% Prepare the figure for plotting the location points
map = figure;
set(map,'visible','off')
% axis([0 xmax 0 ymax])
axis('square')
xlabel('x');
ylabel('y');
hold

plot_pd_target(sensors,z_light,'dg');

% Settings for plots
LineWidth = 1;

for ix = 1:numel(xvals)
    for iy = 1:numel(yvals)

        f = ((ix-1)*numel(yvals) + iy)/(numel(xvals)*numel(yvals));
        fprintf('%3.0f%%\r',f*100)
        % waitbar(f,h);   

        plot(xvals(ix),yvals(iy),'xr','LineWidth',LineWidth);

    	% Position of the light source
    	light_vector = [xvals(ix) yvals(iy) z_light];
    	
        % Received light estimate
    	received_light = pd_rec_estimate_f(sensors,light_vector, Srel, FOV);
    	
        % Estimate of the light source direction
        new_estimate = pd_estimate1(sensors, received_light);
        
        % Angle error is the angle between the actual light source direction
        % and the estimate
        angle_error(iy,ix) = abs(acosd(dot(light_vector,new_estimate)/...
        	(norm(light_vector)*norm(new_estimate))));

        % Estimate the source light position relative to robot
        k = z_light / new_estimate(3);      % k = z_s / z_d

        location =  k * new_estimate;
        this_location_error = location - light_vector;

        % Compute location error
        location_error(iy,ix) = norm(location - light_vector);

        plot(location(1),location(2),'ob','LineWidth',LineWidth);
        coords = sprintf('(%3.1f,%3.1f)',light_vector(1),light_vector(2));
        % text(location(1),location(2),coords);
        h = quiver(xvals(ix),yvals(iy),this_location_error(1),...
            this_location_error(2),'-b');
        set(h,'MaxHeadSize',0.45,'LineWidth',1);
        
        %title(sprintf('z_{light}=%d, FOV=%2.1f^o', ...
        %    z_light, FOV*180/pi));

    end
end

save temp 
% close(h);

% Plot the results
figure;
% Use surf() for visualization
% surf(xvals, yvals,angle_error);
% contourf seems to cause visualization problems 
contourf(xvals, yvals,location_error,0:0.005:0.5);
% title(sprintf('gp3 Angle error plot: z_{light}=%d, N_{Merid}=%d, N_{Paral}=%d, FOV=%2.1f^o', ...
%	z_light, NMerid, NParal,FOV*180/pi));
axis([xmin xmax ymin ymax])
xlabel('x');
ylabel('y');
zlabel('Angle error');
view(0,90);
caxis('manual');
% Set color map limits to 0 and 30:
caxis([0 0.5]);
colorbar();
axis('square');
shading('flat');

hh = fopen('max_error.csv','a');
fprintf(hh,'%d\t%d\n',FOV,max(max(location_error)));
fclose(hh);

return

