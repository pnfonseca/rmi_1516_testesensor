function result = pd_sensor_build2(NMerid, NParal, varargin)
% function result = pd_sensor_build(NMerid, NParal, PMerid, PParal, SMerid)
%
% Creates the array of sensor versors, indicating the directions 
% of the sensors, given the number of meridians (NMerid) and parallels (NParal). 
% The construction does not include the vertical ("north-pole") sensor.
%
% The sensor is a hemi-spherical dome, where photo-diodes are regularly
% placed, over NMerid meridians and NParal paralels. The phase arguments
% are optional; if included, the photodiode location will not start at 
% zero, but at the angle NMerid (for azimuth) and NParal (for elevation).
% Note that the effect of phase is different in the azimuth and elevation.
% In azimuth, the phase causes a shift in the position of all sensors. In 
% elevation, the phase is the position of the least elevated sensor and the
% remaining sensors are equally distributed from the initial point and
% pi/2 (not including)
%
% SMerid is a shift in the meridian values from one paralel to the parallel above. 

% Default values
% Meridian start phase
PMerid = 0;

% Paralel start phase
PParal = 0;

% Meridian shift phase (angle change in longitude going from one parallel to the next) 
SMerid = 0; 


% Processing the optional input arguments
nVarargs = length(varargin);
if (nVarargs >= 1)
	PMerid = varargin{1};
end
if nVarargs >=2
	PParal = varargin{2};
end
if nVarargs >= 3
	SMerid = varargin{3};
end
if nVarargs >= 4
	SParal = varargin{4};
end


result = [];

for m = 0:NMerid-1
	for p = 0:NParal-1
		azimuth = PMerid + m*2*pi/NMerid + p*SMerid;
		elevation = PParal + (p*(pi/2-PParal))/NParal;
		[vx vy vz] = sph2cart(azimuth,elevation,1);
		result = [result ; vx vy vz];
	end
end
