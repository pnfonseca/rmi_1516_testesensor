% Script fot generating surphace graphs for the photo-diode sensor error

z_light = 1;

xmax = 10;
xpoints = 100;
ymax = 10;
ypoints = 100;

xvals = 0:xmax/xpoints:xpoints;
yvals = 0:ymax/ypoints:ypoints;


for ix = 1:numel(xvals)
    for iy = 1:numel(yvals)
    	light_vector = [xvals(ix) yvals(iy) z_light]
        new_estimate = estimate1(light_vector);
        estimates(ix,iy) = new_estimate;
        angle_error(ix,iy) = acosd(dot(light_vector,new_estimate)/(norm(light_vector)*norm(new_estimate)));
    end
end

figure(gcf)
mesh(angulos)
