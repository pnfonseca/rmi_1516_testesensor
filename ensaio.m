function ensaio(luz)

% Vetores dos sensores (fotodíodos)
% Não é considerado o 9.º sensor, para fazer um array regular.
sensores = [1 0 0
			0 1 0
			-1 0 0
			0  -1 0	
			1 0 1
			0 1 1
			-1 0 1
			0 -1 1];

%			0 0 1];

% Normalização do array de sensores (todos os vetores linha têm norma unitária)
norms = sqrt(sum(sensores.^2,2));
norms = norms(:,ones(1,3));
sensores_norm = sensores ./ norms;

% Criação de matriz com a posição da lux e mesma dimensão que a matriz
% com os vetores dos fotodíodos
lux = repmat (luz,size(sensores,1),1);

% Normalização do array com as posiçẽos da luz
norml = sqrt(sum(lux.^2,2));
norml = norml(:,ones(1,3));
lux_norm= lux ./ norml;

% Cálculo dos ângulos de cada sensor para a fonte de luz:
angulos = acosd(dot(sensores_norm,lux_norm,2))

%Estimativa da intensidade de luz recebida por cada sensor
rec = cosd(angulos) .* (angulos < 90);

% Estimativa da posição da luz
estimativa = sum(rec .* sensores_norm);
estimativa = estimativa/norm(estimativa);

% Cálculo do erro na estimativa
dot(luz,estimativa,2);
ang_erro = abs(acosd(dot(luz,estimativa,2)/(norm(luz)*norm(estimativa))));

return