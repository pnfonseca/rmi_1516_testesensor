function graph_plot1(xmax, xpoints, ymax, ypoints, z_light, NMerid, NParal, FOV,p)
% Script fot generating surphace graphs for the photo-diode sensor error. 
% The error is computed by placing the light source on several points 
% in the first quadrant, in an rectangle defined by (0,0)
% and (xmax,ymax). The grid contains xpointx by ypoints. 
% The sensor is placed at (0,0), oriented towards the x axys.
% The light source is placed at the several points in the first quadrant, 
% at a height of z_light. 
% The sensor is build with NMerid meridians and NParal parallels.

% Array with the phodo-diodes orientations. 
sensors = pd_sensor_build(NMerid, NParal);


% Compute the points in the x and y axis:
xvals = 0:xmax/xpoints:xmax;
yvals = 0:ymax/ypoints:ymax;

% Error computation
angle_error=[];

for ix = 1:numel(xvals)
    for iy = 1:numel(yvals)
    	% Position of the light source
    	light_vector = [xvals(ix) yvals(iy) z_light];
    	% Received light estimate
    	received_light = pd_rec_estimate(sensors,light_vector,FOV);
    	% Estimate of the light source direction
        new_estimate = pd_estimate1(sensors, received_light);
        % Error is the angle between the "real" light source direction
        % and the estimate
        angle_error(iy,ix) = abs(acosd(dot(light_vector,new_estimate)/...
        	(norm(light_vector)*norm(new_estimate))));
    end
end

% Plot the results
figure(gcf);
% surf(xvals, yvals,angle_error);
contourf(xvals, yvals,angle_error,0:5:30);
title(sprintf('gp1 Error plot: z_{light}=%d, N_{Merid}=%d, N_{Paral}=%d, FOV=%d^o', ...
	z_light, NMerid, NParal,FOV*180/pi));
axis([0 xmax 0 ymax])
xlabel('x');
ylabel('y');
zlabel('Angle error');
colorbar();
axis('square');

if p>0
    nome = sprintf('Texto/imagens/z%dNP%02dNM%02dFOV%02d.eps',...
        z_light,NParal,NMerid,FOV*180/pi)
    % saveas(gcf,nome)
    print(nome,'-depsc');
endif

% Get the max value for the error and the position 
[ max_error ind ] = max(angle_error(:));
[ r c ] = ind2sub(size(angle_error),ind);

max_error
x_err_max = xvals(r)
y_err_max = yvals(c)
