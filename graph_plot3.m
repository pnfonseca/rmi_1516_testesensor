function graph_plot3(xmax, xpoints, ymax, ypoints, z_light, NMerid, NParal, Srel, FOV,p)
% Script fot generating surphace graphs for the location sensor error
% 
%.Created from graph_plot2.m 
%
% Plots the location error (graph_plot2.m plots the direction error).
%  
% The error is computed by placing the light source on several points 
% in the first quadrant, in an rectangle defined by (0,0)
% and (xmax,ymax). The grid contains xpointx by ypoints. 
% The sensor is placed at (0,0), oriented towards the x axys.
% The light source is placed at the several points in the first quadrant, 
% at a height of z_light. 
% The sensor is build with NMerid meridians and NParal parallels.


% Array with the phodo-diodes orientations. 
sensors = pd_sensor_build(NMerid, NParal);

% Compute the points in the x and y axis:
xvals = 0:xmax/xpoints:xmax;
yvals = 0:ymax/ypoints:ymax;

% Error computation
angle_error=[];
location_error=[];

for ix = 1:numel(xvals)
    for iy = 1:numel(yvals)

    	% Position of the light source
    	light_vector = [xvals(ix) yvals(iy) z_light];
    	
        % Received light estimate
    	received_light = pd_rec_estimate_f(sensors,light_vector, Srel, FOV);
    	
        % Estimate of the light source direction
        new_estimate = pd_estimate1(sensors, received_light);
        
        % Angle error is the angle between the actual light source direction
        % and the estimate
        angle_error(iy,ix) = abs(acosd(dot(light_vector,new_estimate)/...
        	(norm(light_vector)*norm(new_estimate))));

        % Compute location
        k = z_light / new_estimate(3);      % k = z_s / z_d

        location = light_vector - k * new_estimate;

        % Compute location error
        location_error(iy,ix) = norm(location);
    end
end

% Plot the results
figure(gcf);
% Use surf() for visualization
surf(xvals, yvals,angle_error);
% contourf seems to cause visualization problems 
% contourf(xvals, yvals,angle_error,0:5:30);
title(sprintf('gp3 Angle error plot: z_{light}=%d, N_{Merid}=%d, N_{Paral}=%d, FOV=%2.1f^o', ...
	z_light, NMerid, NParal,FOV*180/pi));
axis([0 xmax 0 ymax])
xlabel('x');
ylabel('y');
zlabel('Angle error');
view(0,90);
caxis('manual');
% Set color map limits to 0 and 30:
caxis([0 30]);
colorbar();
axis('square');
shading('flat');

save('-V7','graph_plot2.mat','angle_error');

% Open diary file
diary 'gp2_output.txt'

if p>0
    nome = sprintf('Texto/imagens/angle_error_z%dNP%02dNM%02dFOV%02d.eps',...
        z_light,NParal,NMerid,FOV*180/pi)
    % saveas(gcf,nome)
    print(nome,'-depsc');
endif

% Get the max value for the angle error and the position 
[ max_angle_error ind ] = max(angle_error(:));
[ r c ] = ind2sub(size(angle_error),ind);

max_angle_error
x_err_max = xvals(r)
y_err_max = yvals(c)

% Relative fraction of non valid results
frac_nan = sum(isnan(angle_error(:) )) /max(size(angle_error(:)))

figure
% Use surf() for visualization

surf(xvals, yvals,location_error);
% contourf seems to cause visualization problems 
% contourf(xvals, yvals,angle_error,0:5:30);
title(sprintf('gp3 Location error plot: z_{light}=%d, N_{Merid}=%d, N_{Paral}=%d, FOV=%2.1f^o', ...
    z_light, NMerid, NParal,FOV*180/pi));
axis([0 xmax 0 ymax])
xlabel('x');
ylabel('y');
zlabel('Angle error');
view(0,90);
% caxis('manual');
% Set color map limits to 0 and 30:
% caxis([0 30]);
colorbar();
axis('square');
shading('flat');



diary off