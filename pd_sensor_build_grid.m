function [ sensors ] = pd_sensor_build( xmin, xmax, xcount, ymin, ymax, ycount, z)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

% Create initial empty vector
sensors = [];

for x = linspace(xmin,xmax,xcount)
    for y = linspace(ymin, ymax, ycount)
        
        % create the vector aiming at [x,y,z]
        this_sensor = [x, y, z];
        
        % Make this_sensor norm equal to 1
        this_sensor = this_sensor / norm(this_sensor);
        
        % Add to the sensor array
        sensors = [ sensors ; this_sensor ];
    end
end
            
end

