% Ensaio para verificar a estimativa de erro direcional 

% Set global conditions (for all tests)

sensors = pd_sensor_build(8,2);

z_light=1;
r_light = 6; 


% Directional sensitivity function
Srel = @s_sfh213

% Field of view
FOV = pi/9

% Position the light source above the x axis
light_source1 = pol2cart(0, r_light, z_light)
% Compute the received power:
e1 = pd_rec_estimate_f(sensors,light_source1, Srel, FOV)

% Compute the direction estimate
estimate1 = pd_estimate1(sensors, e1)

error1 = abs(acosd(dot(light_source1,estimate1)/...
        	(norm(light_source1)*norm(estimate1))))

% Move the light source to over the y axis
% and compute the received power
light_source2 = pol2cart(pi/2, r_light, z_light)
e2 = pd_rec_estimate_f(sensors,light_source2, Srel, FOV)

% Compute the direction estimate
estimate2 = pd_estimate1(sensors, e2)
error2 = abs(acosd(dot(light_source2,estimate2)/...
        	(norm(light_source2)*norm(estimate2))))

% Move the light source to the line x=y, at a distance 1 of
% the origin and compute the received power
light_source3 = pol2cart(pi/4, r_light, z_light)
e3 = pd_rec_estimate_f(sensors,light_source3, Srel, FOV)

% Compute the direction estimate
estimate3 = pd_estimate1(sensors, e3)
error3 = abs(acosd(dot(light_source3,estimate3)/...
        	(norm(light_source3)*norm(estimate3))))


% Plot the received values
figure
plot(e1);
hold on
plot(e2,'r')
plot(e3,'g')
