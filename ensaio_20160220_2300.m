%% Preparation

Ensaio = '20160220'

configs = [	8 2
			16 4
			32 8];
 

%% Variant 1, elevation = 0
Variant = '1';
elevation = 0;
% Build and plot example sensor
sensor = pd_sensor_build1(configs(1,1),configs(1,2),0,elevation);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	


elevation = 0;
for FOV = [ pi/4 pi/6 pi/8 pi/12 ]
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build1(NM,NP,0,elevation);
		graph_plot5(-10, 10, 11, -10, 10, 11, 1, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
	endfor
endfor


%% Variant 1, elevation = 6 deg
Variant = '1';
elevation = 6*pi/180;
% Build and plot example sensor
sensor = pd_sensor_build1(configs(1,1),configs(1,2),0,elevation);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	

for FOV = [ pi/4 pi/6 pi/8 pi/12 ]
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build1(NM,NP,0,elevation);
		graph_plot5(-10, 10, 11, -10, 10, 11, 1, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
	endfor
endfor



%% Variant 1a, elevation = 6deg
Variant = '1a';
elevation = 6*pi/180;
% Build and plot example sensor
sensor = pd_sensor_build1a(configs(1,1),configs(1,2),0,elevation);
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	

for FOV = [ pi/4 pi/6 pi/8 pi/12 ]
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build1a(NM,NP,0,elevation);
		graph_plot5(-10, 10, 11, -10, 10, 11, 1, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
	endfor
endfor

%% Variant 2, elevation = 6deg
Variant = '2';
elevation = 6*pi/180;
% Build and plot example sensor
sensor = pd_sensor_build2(configs(1,1),configs(1,2),0,elevation,pi/configs(1,1));
figure;
plot_pd_sensor(sensor);
title(sprintf('Variant %s, example sensor (PParal=%3.1f)',Variant,elevation*180/pi));	

for FOV = [ pi/4 pi/6 pi/8 pi/12 ]
	for i=1:size(configs,1)
		NM = configs(i,1);
		NP = configs(i,2);
		sensor = pd_sensor_build2(NM,NP,0,elevation,pi/NM);
		graph_plot5(-10, 10, 11, -10, 10, 11, 1, sensor , @s_cos, FOV,0)
		t = sprintf('Variant %s, %dx%d, FOV=%3.1f deg Phase=%3.1f deg',...
			Variant, NM, NP, FOV*180/pi, elevation*180/pi);
		title(t)
		basename = sprintf('E%s/Var%s_%dx%d_%02.0f_%02.0f',...
			Ensaio,Variant,NM,NP,FOV*180/pi,elevation*180/pi);
		print(strcat(basename,'.eps'), '-depsc');
		print(strcat(basename,'.png'), '-dpng');
	endfor
endfor

%% Call HEALPix section

ensaio_20160220_2300a